package com.jossemar.account.service;

import com.jossemar.account.model.Account;
import reactor.core.publisher.Mono;

public interface AccountService {
    Mono<Account> save(Mono<Account> account);
    Mono<Account> findById(String id);
    Mono<Account> update(Mono<Account> account, String id);
    Mono<Void> deleteById(String id);
}
