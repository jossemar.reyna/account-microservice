package com.jossemar.account.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@Document
public class Account {
    @Id
    private String id;
    private String accountNumber;
    private String typeAccount;
    private BigDecimal balance;
    private String titular;
    private Instant OpenDate;
    private double interestRate;
    private List<String> movements;
    private String status;
    @CreatedDate
    private Instant createdAt;
    @LastModifiedDate
    private Instant updatedAt;
}
