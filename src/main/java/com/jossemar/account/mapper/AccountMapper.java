package com.jossemar.account.mapper;

import com.jossemar.account.domain.Account;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    com.jossemar.account.model.Account toModel(Account account);
    Account toDocument(com.jossemar.account.model.Account account);
}
